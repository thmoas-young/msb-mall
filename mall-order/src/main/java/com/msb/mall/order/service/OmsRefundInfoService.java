package com.msb.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.mall.order.entity.OmsRefundInfoEntity;
import com.msb.common.utils.PageUtils;
import java.util.Map;

/**
 * 退款信息
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:19:44
 */
public interface OmsRefundInfoService extends IService<OmsRefundInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

