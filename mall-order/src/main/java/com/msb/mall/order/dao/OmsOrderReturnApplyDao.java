package com.msb.mall.order.dao;

import com.msb.mall.order.entity.OmsOrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:19:44
 */
@Mapper
public interface OmsOrderReturnApplyDao extends BaseMapper<OmsOrderReturnApplyEntity> {
	
}
