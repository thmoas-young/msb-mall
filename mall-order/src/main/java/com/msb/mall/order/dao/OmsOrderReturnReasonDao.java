package com.msb.mall.order.dao;

import com.msb.mall.order.entity.OmsOrderReturnReasonEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退货原因
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:19:44
 */
@Mapper
public interface OmsOrderReturnReasonDao extends BaseMapper<OmsOrderReturnReasonEntity> {
	
}
