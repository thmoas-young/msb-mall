package com.msb.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.mall.order.entity.OmsOrderItemEntity;
import com.msb.common.utils.PageUtils;
import java.util.Map;

/**
 * 订单项信息
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:19:44
 */
public interface OmsOrderItemService extends IService<OmsOrderItemEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

