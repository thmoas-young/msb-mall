package com.msb.mall.product.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SkuInfoVo {

    private String spuName;

    private String spuDescription;

    private long catalogId;

    private long brandId;

    private BigDecimal weight;

    private BigDecimal publishStatus;

    private List<String > decript;

    private List<String> images;

    private Bounds bounds;

    private List<BaseAttrs> baseAttrs;

    private List<Skus> skus;
}
