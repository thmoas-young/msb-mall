package com.msb.mall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.common.utils.PageUtils;
import com.msb.mall.product.entity.SpuInfoEntity;
import com.msb.mall.product.vo.SpuInfoVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-19 13:50:15
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuInfoVo spuInfoVo);

    PageUtils queryPageByCondition(Map<String, Object> params);
}

