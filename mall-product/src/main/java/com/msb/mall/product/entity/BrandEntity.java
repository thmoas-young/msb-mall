package com.msb.mall.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.msb.common.exception.groups.AddGroupInterface;
import com.msb.common.exception.groups.UpdateGroupInterface;
import com.msb.common.valid.ListValue;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 品牌
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-19 13:50:16
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@TableId
	@NotNull(message = "更新数据品牌ID必须不为空",groups = {UpdateGroupInterface.class})
	@Null(message = "更新数据品牌ID必须为空",groups = {AddGroupInterface.class})
	private Long brandId;
	/**
	 * 品牌名
	 */
	@NotBlank(message = "品牌的名称不能为空",groups = {AddGroupInterface.class})
	private String name;
	/**
	 * 品牌logo地址
	 */
	@NotBlank(message = "logo不能为空",groups = {AddGroupInterface.class})
	@URL(message = "logo必须是一个合法URL地址",groups = {UpdateGroupInterface.class,AddGroupInterface.class})
	private String logo;
	/**
	 * 介绍
	 */
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotNull(message = "显示状态不能为空",groups = AddGroupInterface.class)
	@ListValue(val={0,1},groups = {AddGroupInterface.class,UpdateGroupInterface.class})
	private Integer showStatus;
	/**
	 * 检索首字母
	 */
	@NotBlank(message = "检索首字母不能为空",groups = AddGroupInterface.class)
	@Pattern(regexp = "^[a-zA-Z]$",message = "检索首字母必须是单个的字母",groups = {UpdateGroupInterface.class,AddGroupInterface.class})
	private String firstLetter;
	/**
	 * 排序
	 */
	@NotNull(message = "排序不能为null",groups = AddGroupInterface.class)
	@Min(value = 0,message = "排序不能小于0",groups = {UpdateGroupInterface.class,AddGroupInterface.class})
	private Integer sort;

}
