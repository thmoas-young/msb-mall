package com.msb.mall.product.dao;

import com.msb.mall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-19 13:50:15
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {
	
}
