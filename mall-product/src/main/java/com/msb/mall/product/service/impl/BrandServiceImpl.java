package com.msb.mall.product.service.impl;

import com.msb.common.utils.PageUtils;
import com.msb.mall.product.dao.CategoryBrandRelationDao;
import com.msb.mall.product.service.CategoryBrandRelationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.msb.common.utils.Query;

import com.msb.mall.product.dao.BrandDao;
import com.msb.mall.product.entity.BrandEntity;
import com.msb.mall.product.service.BrandService;
import org.springframework.transaction.annotation.Transactional;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {


    @Autowired
    @Lazy
    CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    CategoryBrandRelationDao relationDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        QueryWrapper<BrandEntity> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(key)){
            //添加对应的条件查询
            wrapper.eq("brand_id",key).or().like("name",key);
        }
        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void updateDetail(BrandEntity brand) {
        // 1.更新原始数据
        this.updateById(brand);
        if(!StringUtils.isEmpty(brand.getName())){
            // 同步更新级联关系中的数据
            categoryBrandRelationService.updateBrandName(brand.getBrandId(),brand.getName());
            // TODO 同步更新其他的品牌名称的冗余数据

        }
    }

}