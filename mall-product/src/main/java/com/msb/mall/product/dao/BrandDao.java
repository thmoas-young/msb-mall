package com.msb.mall.product.dao;

import com.msb.mall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-19 13:50:16
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
