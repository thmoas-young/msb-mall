package com.msb.mall.ware.dao;

import com.msb.mall.ware.entity.WmsWareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:42:04
 */
@Mapper
public interface WmsWareInfoDao extends BaseMapper<WmsWareInfoEntity> {
	
}
