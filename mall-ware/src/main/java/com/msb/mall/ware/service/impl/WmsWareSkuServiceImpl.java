package com.msb.mall.ware.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.msb.mall.ware.dao.WmsWareSkuDao;
import com.msb.mall.ware.entity.WmsWareSkuEntity;
import com.msb.mall.ware.service.WmsWareSkuService;


@Service("wmsWareSkuService")
public class WmsWareSkuServiceImpl extends ServiceImpl<WmsWareSkuDao, WmsWareSkuEntity> implements WmsWareSkuService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
//        IPage<WmsWareSkuEntity> page = this.page(
//                new Query<WmsWareSkuEntity>().getPage(params),
//                new QueryWrapper<WmsWareSkuEntity>()
//        );
//
//        return new PageUtils(page);
        return new PageUtils(new ArrayList<>(), 0, 0,0);
    }

}