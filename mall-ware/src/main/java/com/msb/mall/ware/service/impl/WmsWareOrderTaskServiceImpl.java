package com.msb.mall.ware.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.msb.mall.ware.dao.WmsWareOrderTaskDao;
import com.msb.mall.ware.entity.WmsWareOrderTaskEntity;
import com.msb.mall.ware.service.WmsWareOrderTaskService;


@Service("wmsWareOrderTaskService")
public class WmsWareOrderTaskServiceImpl extends ServiceImpl<WmsWareOrderTaskDao, WmsWareOrderTaskEntity> implements WmsWareOrderTaskService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
//        IPage<WmsWareOrderTaskEntity> page = this.page(
//                new Query<WmsWareOrderTaskEntity>().getPage(params),
//                new QueryWrapper<WmsWareOrderTaskEntity>()
//        );
//
//        return new PageUtils(page);
        return new PageUtils(new ArrayList<>(), 0, 0,0);
    }

}