package com.msb.mall.ware.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.msb.mall.ware.dao.WmsPurchaseDetailDao;
import com.msb.mall.ware.entity.WmsPurchaseDetailEntity;
import com.msb.mall.ware.service.WmsPurchaseDetailService;


@Service("wmsPurchaseDetailService")
public class WmsPurchaseDetailServiceImpl extends ServiceImpl<WmsPurchaseDetailDao, WmsPurchaseDetailEntity> implements WmsPurchaseDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
//        IPage<WmsPurchaseDetailEntity> page = this.page(
//                new Query<WmsPurchaseDetailEntity>().getPage(params),
//                new QueryWrapper<WmsPurchaseDetailEntity>()
//        );
//
//        return new PageUtils(page);
        return new PageUtils(new ArrayList<>(), 0, 0,0);
    }

}