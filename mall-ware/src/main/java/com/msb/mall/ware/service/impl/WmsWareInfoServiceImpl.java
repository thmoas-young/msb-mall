package com.msb.mall.ware.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.msb.mall.ware.dao.WmsWareInfoDao;
import com.msb.mall.ware.entity.WmsWareInfoEntity;
import com.msb.mall.ware.service.WmsWareInfoService;


@Service("wmsWareInfoService")
public class WmsWareInfoServiceImpl extends ServiceImpl<WmsWareInfoDao, WmsWareInfoEntity> implements WmsWareInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
//        IPage<WmsWareInfoEntity> page = this.page(
//                new Query<WmsWareInfoEntity>().getPage(params),
//                new QueryWrapper<WmsWareInfoEntity>()
//        );
//
//        return new PageUtils(page);
        return new PageUtils(new ArrayList<>(), 0, 0,0);
    }

}