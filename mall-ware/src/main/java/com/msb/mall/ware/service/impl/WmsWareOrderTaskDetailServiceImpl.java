package com.msb.mall.ware.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.msb.mall.ware.dao.WmsWareOrderTaskDetailDao;
import com.msb.mall.ware.entity.WmsWareOrderTaskDetailEntity;
import com.msb.mall.ware.service.WmsWareOrderTaskDetailService;


@Service("wmsWareOrderTaskDetailService")
public class WmsWareOrderTaskDetailServiceImpl extends ServiceImpl<WmsWareOrderTaskDetailDao, WmsWareOrderTaskDetailEntity> implements WmsWareOrderTaskDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
//        IPage<WmsWareOrderTaskDetailEntity> page = this.page(
//                new Query<WmsWareOrderTaskDetailEntity>().getPage(params),
//                new QueryWrapper<WmsWareOrderTaskDetailEntity>()
//        );
//
//        return new PageUtils(page);
        return new PageUtils(new ArrayList<>(), 0, 0,0);
    }

}