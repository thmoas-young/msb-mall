package com.msb.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.mall.member.entity.UmsIntegrationChangeHistoryEntity;
import com.msb.common.utils.PageUtils;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:31:50
 */
public interface UmsIntegrationChangeHistoryService extends IService<UmsIntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

