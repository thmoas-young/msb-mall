package com.msb.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.mall.member.entity.UmsMemberStatisticsInfoEntity;
import com.msb.common.utils.PageUtils;

import java.util.Map;

/**
 * 会员统计信息
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:31:50
 */
public interface UmsMemberStatisticsInfoService extends IService<UmsMemberStatisticsInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

