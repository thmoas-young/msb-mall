package com.msb.mall.member.dao;

import com.msb.mall.member.entity.UmsIntegrationChangeHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 积分变化历史记录
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:31:50
 */
@Mapper
public interface UmsIntegrationChangeHistoryDao extends BaseMapper<UmsIntegrationChangeHistoryEntity> {
	
}
