package com.msb.mall.member.dao;

import com.msb.mall.member.entity.UmsMemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:31:50
 */
@Mapper
public interface UmsMemberCollectSubjectDao extends BaseMapper<UmsMemberCollectSubjectEntity> {
	
}
