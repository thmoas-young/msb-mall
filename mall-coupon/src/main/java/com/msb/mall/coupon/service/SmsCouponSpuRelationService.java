package com.msb.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.mall.coupon.entity.CouponSpuRelationEntity;
import com.msb.common.utils.PageUtils;
import java.util.Map;

/**
 * 优惠券与产品关联
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:22:32
 */
public interface SmsCouponSpuRelationService extends IService<CouponSpuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

