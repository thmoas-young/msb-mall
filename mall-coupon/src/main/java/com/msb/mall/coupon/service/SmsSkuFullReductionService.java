package com.msb.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.common.dto.SkuReductionDTO;
import com.msb.mall.coupon.entity.SkuFullReductionEntity;
import com.msb.common.utils.PageUtils;
import java.util.Map;

/**
 * 商品满减信息
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:22:32
 */
public interface SmsSkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuReduction(SkuReductionDTO dto);
}

