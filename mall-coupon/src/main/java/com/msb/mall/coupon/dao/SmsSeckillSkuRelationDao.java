package com.msb.mall.coupon.dao;

import com.msb.mall.coupon.entity.SeckillSkuRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动商品关联
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:22:32
 */
@Mapper
public interface SmsSeckillSkuRelationDao extends BaseMapper<SeckillSkuRelationEntity> {
	
}
