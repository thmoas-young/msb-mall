package com.msb.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.mall.coupon.entity.CouponEntity;
import com.msb.common.utils.PageUtils;

import java.util.Map;

/**
 * 优惠券信息
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:22:32
 */
public interface SmsCouponService extends IService<CouponEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

