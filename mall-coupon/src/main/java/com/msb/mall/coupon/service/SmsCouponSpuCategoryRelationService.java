package com.msb.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.mall.coupon.entity.CouponSpuCategoryRelationEntity;
import com.msb.common.utils.PageUtils;
import java.util.Map;

/**
 * 优惠券分类关联
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:22:32
 */
public interface SmsCouponSpuCategoryRelationService extends IService<CouponSpuCategoryRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

