package com.msb.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.mall.coupon.entity.SeckillPromotionEntity;
import com.msb.common.utils.PageUtils;
import java.util.Map;

/**
 * 秒杀活动
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:22:32
 */
public interface SmsSeckillPromotionService extends IService<SeckillPromotionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

