package com.msb.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msb.mall.coupon.entity.SeckillSkuNoticeEntity;
import com.msb.common.utils.PageUtils;
import java.util.Map;

/**
 * 秒杀商品通知订阅
 *
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:22:32
 */
public interface SmsSeckillSkuNoticeService extends IService<SeckillSkuNoticeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

