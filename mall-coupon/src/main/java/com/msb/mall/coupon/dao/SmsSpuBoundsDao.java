package com.msb.mall.coupon.dao;

import com.msb.mall.coupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author jlyang
 * @email 2272867453@qq.com
 * @date 2024-01-22 22:22:32
 */
@Mapper
public interface SmsSpuBoundsDao extends BaseMapper<SpuBoundsEntity> {
	
}
